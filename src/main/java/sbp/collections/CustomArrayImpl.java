package sbp.collections;

import java.util.*;

/**
 * Класс CustomArrayImpl представляет собой динамический массив с элементами любого типа
 *
 * @param <T> type of array
 */
public class CustomArrayImpl<T> implements CustomArray <T>
{

    private T[] elements;
    private int size;

    /**
     *  конструктор метода
     */
    public CustomArrayImpl()
    {
        this.elements = (T[])new Object[10];
        this.size = 0;
    }

    /**
     * конструктор коллекции
     * @param elements
     */
    public CustomArrayImpl(Collection<T> elements)
    {
        this(elements.size());
        elements.toArray(this.elements);
        this.size = elements.size();
    }

    /**
     * конструктор размерности
     * @param capacity
     */
    public CustomArrayImpl(int capacity)
    {
        this.elements = (T[])new Object[capacity];
        this.size = 0;
    }



    /**
     * определяем размерность
     *
     * @return размер массива
     */
    @Override
    public int size()
    {
        return this.size;
    }

    /**
     * проверка массива на заполнение
     *
     * @return true если массив пустой или false если массив заполнен
     */
    @Override
    public boolean isEmpty()
    {
        if (this.size == 0)
        {
            return true;
        } else
        {
            return false;
        }
    }

    /**
     * добавление элемента к массиву
     *
     * @param item добавлен
     */
    @Override
    public boolean add(T item)
    {
        if (Objects.nonNull(item))
        {

            ensureCapacity(this.size + 1);
            this.elements[this.size++] = item;
            return true;
        } else
        {
            throw new IllegalArgumentException("parameter items is null");
        }
    }

    /**
     * Добавляем все элементы к массиву
     *
     * @param items массив добавлен
     * @throws IllegalArgumentException если добавляемый массив null
     */
    @Override
    public boolean addAll(T[] items)
    {
        if (Objects.nonNull(items))
        {

            ensureCapacity(this.size + items.length);
            System.arraycopy(this.elements, 0, this.elements, items.length, this.size);

            System.arraycopy(items, 0, this.elements, this.size,
                    items.length);
            this.size += items.length;
            return true;
        } else
        {
            throw new IllegalArgumentException("parameter items is null");
        }
    }

    /**
     * Add all items.
     *
     * @param items collection of adding values
     * @throws IllegalArgumentException if parameter items is null
     */
    @Override
    public boolean addAll(Collection<T> items)
    {

        if (!items.isEmpty())
        {

            T[] temp = items.toArray((T[])new Object[items.size()]);
            return addAll(temp);
        } else
        {
            throw new IllegalArgumentException("parameter items is null");
        }
    }

    /**
     * Добалвение массива с указанного индекса
     *
     * @param index - index
     * @param items - items место для добавления
     * @throws ArrayIndexOutOfBoundsException если индекс находится за пределами границ
     * @throws IllegalArgumentException       параметр места NULL
     */
    @Override
    public boolean addAll(int index, T[] items)
    {
        if (index > this.size || index < 0)
        {
            throw new IndexOutOfBoundsException("index is out of bounds");
        }
        else if (Objects.nonNull(items))
        {

            ensureCapacity(this.size + items.length);
            System.arraycopy(this.elements, index, this.elements, index + items.length,
                    this.size - index);

            System.arraycopy(items, 0, elements, index,
                    items.length);
            this.size += items.length;
            return true;
        } else
        {
            throw new IllegalArgumentException("parameter items is null");
        }
    }

    /**
     * Вызов  элемента по индексу
     *
     * @param index - индекс
     * @throws ArrayIndexOutOfBoundsException если индекс находится за пределами границ
     */
    @Override
    public T get(int index)
    {
        if (index >= this.size || index < 0)
        {
            throw new IndexOutOfBoundsException("index is out of bounds");
        } else
        {
            return elements[index];
        }
    }

    /**
     * Вывод элемента по индексу
     *
     * @param index - интекс
     * @param item
     * @return значение
     * @throws ArrayIndexOutOfBoundsException если индекс находится за пределами границ
     */
    @Override
    public T set(int index, T item)
    {
        if (index >= this.size || index < 0)
        {
            throw new IndexOutOfBoundsException("index is out of bounds");
        } else
        {
            T temp = this.elements[index];
            this.elements[index] = item;
            return temp;
        }
    }

    /**
     * Remove item by index.
     *
     * @param index - index of element
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */
    @Override
    public void remove(int index)
    {
        if (index >= this.size || index < 0)
        {
            throw new IndexOutOfBoundsException("index is out of bounds");
        } else {
            T[] temp = (T[])new Object[this.size - 1];
            for (int i = 0; i < index; i++)
            {
                temp[i] = this.elements[i];
            }
            for (int i = index; i < this.size - 1; i++)
            {
                temp[i] = this.elements[i + 1];
            }
            this.elements = temp;
            this.size = this.size - 1;
        }
    }

    /**
     * Удаляет первое указанное значение. .
     *
     * @param item - значение
     * @return true если элемент был удален
     */
    @Override
    public boolean remove(T item)
    {
        int ind = indexOf(item);
        if (ind != -1)
        {
            remove(ind);
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * Проверяет, существует ли элемент
     *
     * @param item - item
     * @return true or false
     */
    @Override
    public boolean contains(T item)
    {
        for (int i = 0; i < this.size; i++)
        {

            if (this.elements[i].equals(item))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Возвращает индекс заданного элемента
     *
     * @param item - item
     * @return индекс элемента или -1 если список не сожержит элемент
     */
    @Override
    public int indexOf(T item)
    {
        for (int i = 0; i < this.size; i++)
        {

            if (this.elements[i].equals(item))
            {
                return i;
            }
        }
        return -1;
    }

    /**
     * Увеличивает размер массива.
     *
     * @param newElementsCount - количество новых элементов
     */
    @Override
    public void ensureCapacity(int newElementsCount)
    {
        if (newElementsCount > this.elements.length)
        {
            T[] temp = this.elements;
            int maxValue = this.size > newElementsCount ? this.size : newElementsCount;
            this.elements = (T[])new Object[(maxValue * 3) / 2 + 1];
            System.arraycopy(temp, 0, this.elements, 0, this.size);
        }
    }

    /**
     * Возвращает емкость массива
     */
    @Override
    public int getCapacity()
    {
        return this.elements.length;
    }

    /**
     * Возвращает массив с положениями элементов наоборот
     */
    @Override
    public void reverse()
    {

        for (int i = 0, j = this.size - 1; i < this.size / 2; i++, j--)
        {
            T tmp = this.elements[j];
            this.elements[j] = this.elements[i];
            this.elements[i] = tmp;
        }
    }

    /**
     * Получить копию текущего массива.
     */
    @Override
    public Object[] toArray()
    {
        Object[] obj = new Object[this.size];
        System.arraycopy(this.elements, 0, obj, 0,
                this.size);
        return obj;
    }

    /**
     * Вывод на консоль
     *
     * @return String elements of array
     */

    public String toString()
    {
        String str = "";
        for (T el: elements) {
            if (el != null) {

                str += "[" + el.toString() + "] ";
            }
        }
        return str;
    }
}