package sbp.collections;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CustomArrayImplTest  {

    CustomArrayImpl <Integer> customImpl = new CustomArrayImpl<Integer>();

    /**
     * Testing method size
     */
    @Test
    public void testSize() {
        CustomArrayImpl customImpl = new CustomArrayImpl<>();
        customImpl.add(4);
        assertTrue(customImpl.size() >= 0);
        customImpl.add(15);
        assertSame(customImpl.get(0), (Integer) 4);
        customImpl.add("33");
        assertFalse(customImpl.isEmpty());
        assertEquals(3, customImpl.size());
        assertSame("33", customImpl.get(2));
    }

    /**
     * Testing method isEmpty
     */
    @Test
    public void isEmpty() {
        assertTrue(customImpl.isEmpty());
        customImpl.add(4);
        assertFalse(customImpl.isEmpty());
        customImpl.remove((Integer) 4);
        assertTrue(customImpl.isEmpty());
        customImpl.add(5);
        customImpl.add(78);
        customImpl.add(9);
        customImpl.remove(0);
        customImpl.remove(1);
        customImpl.remove(0);
        assertTrue(customImpl.isEmpty());
    }

    /**
     * Testing method add
     */
    @Test
    public void add() {
        CustomArrayImpl customImpl = new CustomArrayImpl<>();
        assertTrue(customImpl.add(4));
        assertEquals(1, customImpl.size());
        assertTrue(customImpl.add(15));
        assertSame(customImpl.get(0), (Integer) 4);
        assertTrue(customImpl.add("33"));
        assertFalse(customImpl.isEmpty());
        assertEquals(3, customImpl.size());
        assertSame("33", customImpl.get(2));
        assertThrows(IllegalArgumentException.class,() -> customImpl.add(null));
    }

    /**
     * Testing method addAll
     * adding array
     */
    @Test
    public void testAddAll1() {
        assertTrue(customImpl.isEmpty());
        customImpl.add(45);
        assertEquals(1, customImpl.size());
        Integer[] arr = {8, 5, 3, 45, 8, 6};
        assertTrue(customImpl.addAll(arr));
        customImpl.add(12);
        assertEquals(8, customImpl.size());
        assertEquals(8, (int) customImpl.get(5));
        Integer[] arra2 = null;
        assertThrows(IllegalArgumentException.class,() -> customImpl.addAll(arra2));
    }

    /**
     * Testing method addAll
     * adding collection
     */
    @Test
    public void testAddAll2() {
        assertTrue(customImpl.isEmpty());
        customImpl.add(45);
        Integer[] arr = {8, 5, 3, 45, 8, 6};
        assertTrue(customImpl.addAll(Arrays.asList(arr)));
        customImpl.add(12);
        assertEquals(8, customImpl.size());
        assertEquals(8, (int) customImpl.get(5));
        List <Integer> collection = new ArrayList<>();
        assertThrows(IllegalArgumentException.class,() -> customImpl.addAll(collection));
    }

    /**
     * Testing method addAll
     * adding array from index
     */
    @Test
    public void testAddAll3() {
        assertTrue(customImpl.isEmpty());
        customImpl.add(45);
        Integer[] arr = {8, 5, 3, 45, 8, 6};
        assertTrue(customImpl.addAll(1, arr));
        customImpl.add(12);
        assertEquals(8, customImpl.size());
        assertEquals(8, (int) customImpl.get(5));
        Integer[] arr1 = null;
        assertThrows(IllegalArgumentException.class,() -> customImpl.addAll(1, arr1));
        assertThrows(IndexOutOfBoundsException.class,() -> customImpl.addAll(9, arr));
    }

    /**
     * Testing method get
     * get from array list from index
     */
    @Test
    public void testGet() {
        assertThrows(IndexOutOfBoundsException.class,() -> customImpl.get(1));
        customImpl.add(45);
        customImpl.add(4);
        assertEquals(4, (int) customImpl.get(1));
    }

    /**
     * Testing method set,
     * setting value to index
     */
    @Test
    public void testSet() {
        assertThrows(IndexOutOfBoundsException.class,() -> customImpl.set(1, 12));
        customImpl.add(7);
        customImpl.add(6);
        assertEquals(6, (int) customImpl.set(1, 12));
        assertThrows(IndexOutOfBoundsException.class,() -> customImpl.set(2, 5));
    }

    /**
     * Testing method remove,
     * remove from index
     */
    @Test
    public void testRemove1() {
        assertThrows(IndexOutOfBoundsException.class,() -> customImpl.remove(1));
        customImpl.add(7);
        customImpl.add(6);
        customImpl.add(15);
        assertEquals(3, customImpl.size());
        customImpl.remove(1);
        assertEquals(2, customImpl.size());
        assertThrows(IndexOutOfBoundsException.class,() -> customImpl.remove(2));
    }

    /**
     * Testing method remove,
     * remove by value
     */
    @Test
    public void testRemove2() {
        assertFalse(customImpl.remove((Integer) 6));
        customImpl.add(7);
        customImpl.add(6);
        customImpl.add(15);
        assertEquals(3, customImpl.size());
        assertTrue(customImpl.remove((Integer) 6));
        assertEquals(2, customImpl.size());
        assertFalse(customImpl.remove((Integer) 6));
        assertTrue(customImpl.remove((Integer) 15));
        assertFalse(customImpl.remove((Integer) 6));
        assertTrue(customImpl.remove((Integer) 7));
    }

    /**
     * Testing method contains,
     * contains value to list array
     */
    @Test
    public void testContains() {
        assertFalse(customImpl.contains((Integer) 6));
        customImpl.add(7);
        customImpl.add(6);
        customImpl.add(15);
        assertTrue(customImpl.contains((Integer) 6));
        assertFalse(customImpl.contains((Integer) 12));
        assertTrue(customImpl.contains((Integer) 15));
        assertFalse(customImpl.contains((Integer) 3));
        assertTrue(customImpl.contains((Integer) 7));
    }

    /**
     * Testing method indexOf,
     * return index of list array by value
     */
    @Test
    public void testIndexOf() {
        assertEquals(customImpl.indexOf((Integer) 6), -1);
        customImpl.add(7);
        customImpl.add(6);
        customImpl.add(15);
        assertEquals(1, customImpl.indexOf((Integer) 6));
        assertEquals(customImpl.indexOf((Integer) 12), -1);
        assertEquals(2, customImpl.indexOf((Integer) 15));
        assertEquals(customImpl.indexOf((Integer) 3), -1);
        assertEquals(0, customImpl.indexOf((Integer) 7));
    }

    /**
     * Testing method reverse,
     * reverse of list array
     */
    @Test
    public void testReverse() {
        CustomArrayImpl <String> customImpl = new CustomArrayImpl<>();
        List <String> ok = new ArrayList<>();
        ok.add("(Integer) 1");
        ok.add("(Integer) 2");
        ok.add("(Integer) 3");
        ok.add("(Integer) 4");
        customImpl.addAll(ok);
        customImpl.reverse();
        for (int i = 0, j = ok.size() - 1; i < customImpl.size(); i++, j--)
        {
            assertTrue(customImpl.get(i) == ok.get(j));
        }
    }

    /**
     * Testing method toArray,
     * list array to array
     */
    @Test
    public void toArray() {
        customImpl.add(7);
        customImpl.add(6);
        customImpl.add(15);
        assertTrue(customImpl.toArray()[0].equals(7));
        assertTrue(customImpl.toArray()[1].equals(6));
        assertTrue(customImpl.toArray()[2].equals(15));
        assertTrue(customImpl.toArray().length == 3);
    }

    /**
     * Testing method toString,
     * output list array to string
     */
    @Test
    public void testToString() {
        customImpl.add(7);
        customImpl.add(6);
        customImpl.add(15);
        assertEquals("[7] [6] [15] ", customImpl.toString());
    }
}